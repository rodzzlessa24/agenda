export default {
  subtract(date, days) {
    return this.add(date, -days);
  },

  add(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  },

  addMonths(date, months) {
    const result = new Date(date);
    result.setMonth(result.getMonth() + months);
    return result;
  },

  startOfWeek(date) {
    const idate = new Date(date);
    const diff = idate.getDate() - idate.getDay();
    return new Date(idate.setDate(diff));
  },

  subtractMonths(date, months) {
    const result = new Date(date);
    result.setMonth(result.getMonth() - months);
    return result;
  },

  endOfWeek(date) {
    const idate = new Date(date);
    const first = idate.getDate() - idate.getDay();
    const last = first + 6;
    return new Date(idate.setDate(last));
  },

  endOfMonth(date) {
    const idate = new Date(date);
    return new Date(idate.getFullYear(), (idate.getMonth() +1), 0).getDate();
  },

  startOfMonthDay(date) {
    const idate = new Date(date);
    return new Date(idate.getFullYear(), idate.getMonth(), 1).getDay()
  },

  startOfMonth(date) {
    const idate = new Date(date);
    return new Date(idate.getFullYear(), idate.getMonth(), 1);
  },

  midnight(date) {
    const idate = new Date(date);
    return new Date(idate.setHours(0, 0, 0, 0));
  },

  isWeekend(date) {
    const idate = new Date(date);
    if (idate.getDay() == 0 || idate.getDay() === 6) {
      return true;
    }
    return false;
  },

  isToday(date) {
    const idate = new Date(date);
    const midnight = this.midnight(idate).getTime();
    const todayMidnight = this.midnight(new Date()).getTime();

    return midnight === todayMidnight;
  },

  currentMonth(currentDate,  date){
    const idate = new Date(date)
    const icurrentDate = new Date(currentDate);
    return idate.getMonth() === icurrentDate.getMonth()
  },

  shortDate(date) {
    const idate = new Date(date);
    let month = `${idate.getMonth() + 1}`;
    let day = `${idate.getDate()}`;
    const year = `${idate.getFullYear()}`;

    if (month.toString().length < 2) month = `0${month}`;
    if (day.toString().length < 2) day = `0${day}`;

    return [month, day, year].join("/");
  },

  sqlDate(date) {
    const idate = new Date(date);
    let month = `${idate.getMonth() + 1}`;
    let day = `${idate.getDate()}`;
    const year = `${idate.getFullYear()}`;

    if (month.toString().length < 2) month = `0${month}`;
    if (day.toString().length < 2) day = `0${day}`;

    return [year, month, day].join("-");
  },

  dateName(date) {
    const idate = new Date(date);
    const day = idate.getDay();

    switch (day) {
      case 0:
        return "Sun";
      case 1:
        return "Mon";
      case 2:
        return "Tue";
      case 3:
        return "Wed";
      case 4:
        return "Thu";
      case 5:
        return "Fri";
      case 6:
        return "Sat";
    }
  },

  dateNumber(date) {
    const idate = new Date(date);
    const day = idate.getDate();
    return day.toString().length < 2 ? `0${day}` : `${day}`;
  },

  formatTime(date) {
    const idate = new Date(date);
    let meridiem = "am";

    let hour = idate.getHours();
    if (hour >= 12) meridiem = "pm";
    if (hour > 12) hour -= 12;

    let minute = idate.getMinutes();
    minute = minute.toString().length < 2 ? `0${minute}` : `${minute}`;

    return `${hour}:${minute} ${meridiem}`;
  },

  getMonthName(date) {
    const idate = new Date(date);
    const day = idate.getMonth();

    switch (day) {
      case 0:
        return "January";
      case 1:
        return "Febuary";
      case 2:
        return "March";
      case 3:
        return "April";
      case 4:
        return "May";
      case 5:
        return "June";
      case 6:
        return "July";
      case 7:
        return "August";
      case 8:
        return "September";
      case 9:
        return "October";
      case 10:
        return "November";
      case 11:
        return "December";
    }
  }
};
