export const formatIndexToTime = index => {
  if (index < 12) {
    return index === 0 ? "" : `${index} am`;
  }
  if (index === 12) return "12 pm";
  return `${index - 12} pm`;
};

export const randomBetween = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export const replaceAll = (str, search, replacement) => {
    return str.replace(new RegExp(search, 'g'), replacement);
}