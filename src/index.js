import "./styles/app.scss";
import Calendar from "./calendar";
import dom from "./dom";
import fmt from "./date";
import { replaceAll } from "./util";

const defaultColors = [
  "#6091f5",
  "rgba(247, 123, 97, .8)",
  "rgba(125, 210, 126, .8)",
  "rgba(249, 176, 73, .8)"
];

class App {
  constructor() {
    this.calendar = null;
    this.activeModal = null;
    this.events = [];

    this._addEventListeners();
  }

  run() {
    const calendar = document.getElementById("calendar");
    this.calendar = new Calendar(calendar);
    this.calendar.render();

    this._setTitle();

    const localStrgEvents = localStorage.getItem("events");
    if (localStrgEvents && localStrgEvents !== "") {
      this.events = JSON.parse(localStrgEvents);
      this.calendar.setEvents(this.events);
    }
  }

  _addEventListeners() {
    this._handleDayClick();
    this._handleEventClick();
    this._handleNextClick();
    this._handlePrevClick();
    this._handleWindowClick();
    this._handleSubmitCreate();
    this._handleSubmitUpdate();
    this._handleDeleteEvent();
    this._handleCancelDeleteEvent();
    this._handleConfirmDeleteEvent();
    this._handleMenuClick();
    this._handleMenuItemClick();
    this._handleEventDateChanged();
    this._handleUpdateEventDateChanged();
  }

  _handleEventClick() {
    window.addEventListener("calendar-event-click", e => {
      this._openModal("#updateEventModal");
      const nameInput = dom.getElement(".update-event-name-input");
      const categoryInput = dom.getElement(".update-event-category-input");
      const selectedTimeInput = dom.getElement(".selected-time-input");
      const dateInput = dom.getElement(".update-event-date-input");
      const hiddenSelectedTimeInput = dom.getElement(
        ".hidden-selected-time-input"
      );
      const idInput = dom.getElement(".update-event-id-input");

      nameInput.value = e.detail.event.name;
      categoryInput.value = e.detail.event.category;
      selectedTimeInput.value = fmt.formatTime(e.detail.event.start * 1000);
      hiddenSelectedTimeInput.value = e.detail.event.start;
      idInput.value = e.detail.event.id;
      dateInput.value = fmt.sqlDate(e.detail.event.start * 1000);

      const times = this._getAvailableTimes(e.detail.event.start * 1000);
      const availableTimesEl = dom.getElement(
        ".update-available-times-wrapper"
      );
      availableTimesEl.innerHTML = "";

      times.map((t, i) => {
        this._createTimeBlock(availableTimesEl, t, i, "update-time");
      });
    });
  }

  _handleDayClick() {
    window.addEventListener("calendar-day-click", e => {
      this._openModal("#newEventModal");

      const times = this._getAvailableTimes(e.detail.date);
      this._genEventTimes(times);

      const eventDateInput = dom.getElement(".event-date-input");
      eventDateInput.value = fmt.sqlDate(e.detail.date);
    });
  }

  _handleSubmitCreate() {
    dom.getElement(".add-event-form").addEventListener("submit", e => {
      e.preventDefault();
      const errorsEl = dom.getElement(".add-event-errors");
      errorsEl.style.display = "none";
      errorsEl.innerHTML = "";
      let errors = [];

      const timeVal = dom.getVal("time", true);
      const nameVal = dom.getVal(".event-name-input");
      const categoryVal = dom.getVal(".event-category-input");

      if (timeVal === "") {
        errors.push("Selecting a time is required.");
      }
      if (nameVal === "") {
        errors.push("Event name is required.");
      }
      if (categoryVal === "") {
        errors.push("Event category is required.");
      }

      if (errors.length > 0) {
        errorsEl.style.display = "block";
        errors.map(e => {
          const errEl = dom.createElement("div", "validation-message");
          errEl.innerHTML = e;
          errorsEl.appendChild(errEl);
        });
        return false;
      }

      const event = {
        id: Math.floor(Math.random() * 100000000),
        name: nameVal,
        start: timeVal,
        end: parseInt(timeVal) + 30 * 60, // all events are 30 minutes long
        category: categoryVal,
        color: this._getCategoryColor(categoryVal)
      };
      this.events.push(event);

      localStorage.setItem("events", JSON.stringify(this.events));
      e.target.reset();

      this._closeModal();
      this.calendar.setEvents(this.events);
      this._successMsg("Your event was created!");
    });
  }

  _handleSubmitUpdate() {
    dom.getElement(".update-event-form").addEventListener("submit", e => {
      e.preventDefault();
      const errorsEl = dom.getElement(".update-event-errors");
      errorsEl.style.display = "none";
      errorsEl.innerHTML = "";
      let errors = [];

      let timeVal = dom.getVal("update-time", true);
      const hiddenTimeVal = dom.getVal(".hidden-selected-time-input");
      const nameVal = dom.getVal(".update-event-name-input");
      const categoryVal = dom.getVal(".update-event-category-input");
      const idVal = dom.getVal(".update-event-id-input");

      if (timeVal === "") {
        timeVal = hiddenTimeVal;
      }
      if (nameVal === "") {
        errors.push("Event name is required.");
      }
      if (categoryVal === "") {
        errors.push("Event category is required.");
      }

      if (errors.length > 0) {
        errorsEl.style.display = "block";
        errors.map(e => {
          const errEl = dom.createElement("div", "validation-message");
          errEl.innerHTML = e;
          errorsEl.appendChild(errEl);
        });
        return false;
      }

      const event = this.events.find(e => e.id === parseInt(idVal));
      event.name = nameVal;
      event.category = categoryVal;
      event.start = timeVal;
      event.end = parseInt(timeVal) * (30 * 60);
      event.color = this._getCategoryColor(categoryVal);

      this.events = this.events.filter(e => e.id !== parseInt(idVal));

      this.events.push(event);
      localStorage.setItem("events", JSON.stringify(this.events));
      e.target.reset();

      this._closeModal();
      this.calendar.setEvents(this.events);
      this._successMsg("Your event was updated!");
    });
  }

  _handleDeleteEvent() {
    dom.getElement(".delete-event-btn").addEventListener("click", e => {
      e.preventDefault();
      const idVal = dom.getVal(".update-event-id-input");
      const confirmDelEventIdInput = dom.getElement(
        ".confirm-delete-event-id-input"
      );
      confirmDelEventIdInput.value = idVal;
      this._closeModal();
      this._openModal("#confirmDeleteModal");
    });
  }

  _handleConfirmDeleteEvent() {
    dom
      .getElement(".confirm-delete-event-form")
      .addEventListener("submit", e => {
        e.preventDefault();
        const idVal = dom.getVal(".confirm-delete-event-id-input");

        this.events = this.events.filter(e => e.id !== parseInt(idVal));
        e.target.reset();

        localStorage.setItem("events", JSON.stringify(this.events));

        this._closeModal();
        this.calendar.setEvents(this.events);
        this._successMsg("Your event was deleted!");
      });
  }

  _handleCancelDeleteEvent() {
    dom.getElement(".cancel-delete-event-btn").addEventListener("click", e => {
      e.preventDefault();
      this._closeModal();
    });
  }

  _handleEventDateChanged() {
    dom.getElement(".event-date-input").addEventListener("change", e => {
      const times = this._getAvailableTimes(e.target.value);
      this._genEventTimes(times);
    });
  }

  _handleUpdateEventDateChanged() {
    dom.getElement(".update-event-date-input").addEventListener("change", e => {
      const times = this._getAvailableTimes(e.target.value);
      this._genEventTimes(
        times,
        ".update-available-times-wrapper",
        "update-time"
      );
    });
  }

  _genEventTimes(
    times,
    selector = ".available-times-wrapper",
    nameAttr = "time"
  ) {
    const availableTimesEl = dom.getElement(selector);
    availableTimesEl.innerHTML = "";

    times.map((t, i) => {
      this._createTimeBlock(availableTimesEl, t, i, nameAttr);
    });
  }

  _handleNextClick() {
    dom.getElement(".right-arrow").addEventListener("click", e => {
      this.calendar.next();
      this._setTitle();
    });
  }

  _handlePrevClick() {
    dom.getElement(".left-arrow").addEventListener("click", e => {
      this.calendar.prev();
      this._setTitle();
    });
  }

  _handleWindowClick() {
    window.addEventListener("click", e => {
      if (event.target === this.activeModal) {
        this._closeModal();
      }
      if (!event.target.classList.contains("stay-open")) {
        this._closeMenu();
      }
    });
  }

  _handleMenuClick() {
    dom.getElement(".section-menu").addEventListener("click", e => {
      const menuEl = dom.getElement(".menu");
      if (menuEl.classList.contains("open")) {
        menuEl.classList.remove("open");
        return;
      }
      menuEl.classList.add("open");
    });
  }

  _closeMenu() {
    const menuEl = dom.getElement(".menu");
    if (menuEl.classList.contains("open")) {
      menuEl.classList.remove("open");
      return;
    }
  }

  _handleMenuItemClick() {
    const items = dom.getElements(".menu-item");

    Array.from(items).map(item => {
      item.addEventListener("click", e => {
        const rows = e.target.getAttribute("agenda-rows");
        this._closeMenu();
        this.calendar.render(parseInt(rows));
      });
    });
  }

  _createTimeBlock(availableTimesEl, t, i, nameAttr = "time") {
    const timeLabelEl = dom.createElement("label", "available-time-block");
    timeLabelEl.innerHTML = fmt.formatTime(t * 1000);
    timeLabelEl.setAttribute("for", `${nameAttr}${i}`);
    const timeCheckboxEl = dom.createElement("input", "available-time-block");
    timeCheckboxEl.setAttribute("type", "radio");
    timeCheckboxEl.setAttribute("name", nameAttr);
    timeCheckboxEl.setAttribute("value", t);
    timeCheckboxEl.style.display = "none";
    timeCheckboxEl.id = `${nameAttr}${i}`;
    availableTimesEl.appendChild(timeCheckboxEl);
    availableTimesEl.appendChild(timeLabelEl);
  }

  _getAvailableTimes(date) {
    // this is for some weird bug happening with JavaScript's date object.
    if (date.toString().includes('-')) {
      const indvDates = date.toString().split('-');
      const newDate = [indvDates[1], indvDates[2], indvDates[0]].join("/");
      date = new Date(newDate);
    }
    let times = [];
    const midnight = fmt.midnight(date).getTime() / 1000;

    const nextDayMidnight = fmt.add(midnight * 1000, 1).getTime() / 1000;

    let currTime = midnight;

    while (currTime < nextDayMidnight) {
      let isAvailable = true;
      this.events.map(e => {
        if (currTime == e.start) {
          isAvailable = false;
        }
      });

      if (isAvailable) times.push(currTime);

      // 30 minute windows for times.
      currTime += 30 * 60;
    }

    return times;
  }

  _successMsg(msg) {
    const textEl = dom.getElement(".success-modal-text");
    textEl.innerHTML = msg;
    this._openModal("#successModal");

    setTimeout(() => {
      this._closeModal();
    }, 1500);
  }

  _closeModal() {
    this.activeModal.style.display = "none";
  }

  _openModal(modal) {
    this.activeModal = dom.getElement(modal);
    this.activeModal.style.display = "flex";
  }

  _getCategoryColor(category) {
    let color;
    switch (category) {
      case "medical":
        color = defaultColors[0];
        break;
      case "sport":
        color = defaultColors[1];
        break;
      case "misc":
        color = defaultColors[2];
        break;
      case "party":
        color = defaultColors[3];
        break;
    }
    return color;
  }

  _setTitle() {
    dom.getElement(".section-title").innerHTML = this.calendar.getTitle();
  }
}

const app = new App();
app.run();
