import Agenda from './agenda'
import Month from './month'

export default class Calendar {
    constructor(element) {
        this.view = 'month'
        this.date = new Date();
        this.element = element;
        this.events = [];
        this.agendaBlockHeight = 49;
        this.renderer = null
        window.innerWidth < 600 ?
            (this.agendaColumns = 3) :
            (this.agendaColumns = 7);
    }

    render(view, columns) {
        this.view = view ? view : this.view;
        this.agendaColumns = columns ? columns : this.agendaColumns

        if (this.view === 'agenda') {
            this.renderer = new Agenda(this.element);
            this.renderer.render(this.agendaColumns);
            if (this.events.length > 0) this.renderer.setEvents(this.events)
            return
        }

        this.renderer = new Month(this.element, this.date, this.events);
        this.renderer.render();
    }

    getTitle() {
        return this.renderer.getTitle()
    }

    prev() {
        this.date = this.renderer.prev()
    }

    next() {
        this.date = this.renderer.next()
    }

    setEvents(events) {
        this.events = events;
        this.renderer.setEvents(this.events)
    }
}