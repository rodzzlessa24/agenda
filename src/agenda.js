import dom from "./dom";
import { formatIndexToTime, randomBetween } from "./util";
import fmt from "./date";

class Agenda {
  constructor(element) {
    this.date = new Date();
    this.element = element;
    this.events = [];
    this.agendaBlockHeight = 49;
    this.agendaColumns = 7
  }

  render(rows) {
    if (rows) this.agendaColumns = rows;
    this.element.innerHTML = "";
    const wrapper = dom.createElement("div", "ta-calendar");
    this.element.appendChild(wrapper);

    const agendaWrapperEl = dom.createElement(
      "div",
      "ta-agenda-calendar-wrapper"
    );
    wrapper.appendChild(agendaWrapperEl);

    this._createLayout(agendaWrapperEl);
  }

  prev() {
    if (this.agendaColumns === 7) {
      const d = fmt.endOfWeek(this.date);
      this.date = fmt.subtract(d, 8);
      return this.render();
    }
    this.date = fmt.subtract(this.date, this.agendaColumns);
    this.render();
    return this.date;
  }

  next() {
    if (this.agendaColumns === 7) {
      const d = fmt.startOfWeek(this.date);
      this.date = fmt.add(d, 8);
      return this.render();
    }
    this.date = fmt.add(this.date, this.agendaColumns);
    this.render();
    return this.date;
  }

  setEvents(events) {
    this.events = events ? events : [];
    this.render();
  }

  getTitle() {
    const firstDay = fmt.startOfWeek(this.date);
    const lastDay = fmt.add(firstDay, this.agendaColumns);
    const fdYear = firstDay.getFullYear();
    const ldYear = lastDay.getFullYear();

    const fdFmtd = `${fmt.getMonthName(firstDay)} ${fdYear}`;
    const ldFmtd = `${fmt.getMonthName(lastDay)} ${ldYear}`;

    if (fdFmtd === ldFmtd) {
      return fdFmtd;
    }

    const shortFdFmtd = fdFmtd.substr(0, 3);
    const shortLdFmtd = ldFmtd.substr(0, 3);

    if (fdYear === ldYear) {
      return `${shortFdFmtd} - ${shortLdFmtd} ${ldYear}`;
    }

    return `${shortFdFmtd} ${fdYear} - ${shortLdFmtd} ${ldYear}`;
  }

  _createLayout(agendaWrapperEl) {
    this._createHeader(agendaWrapperEl);
    this._createBody(agendaWrapperEl);
  }

  _createHeader(agendaWrapperEl) {
    const headerEl = dom.createElement("div", "agenda-header");
    const hourBlockPaddingEl = dom.createElement(
      "div",
      "agenda-header-hour-block-padding"
    );
    headerEl.appendChild(hourBlockPaddingEl);
    agendaWrapperEl.appendChild(headerEl);

    const initialDate = this._initialDate();
    Array(this.agendaColumns)
      .fill()
      .map((_, i) => {
        const date = fmt.add(initialDate, i);
        this._createHeaderLabels(headerEl, date);
      });
  }

  _createHeaderLabels(headerEl, date) {
    const rowEl = dom.createElement("div", "agenda-header-row");
    rowEl.setAttribute("date", fmt.shortDate(date));
    rowEl.innerHTML = `
      <div class="header-date-name">${fmt.dateName(date)}</div>
      <div class="header-date-number">${fmt.dateNumber(date)}</div>
    `;
    headerEl.appendChild(rowEl);
  }

  _createBody(agendaWrapperEl) {
    const bodyEl = dom.createElement("div", "agenda-body");
    agendaWrapperEl.appendChild(bodyEl);

    this._createSideHours(bodyEl);
    this._createAgendaDisplay(bodyEl);
  }

  _createSideHours(bodyEl) {
    const timeWrapperEl = dom.createElement("div", "agenda-hours-wrapper");
    bodyEl.appendChild(timeWrapperEl);

    Array(24)
      .fill()
      .map((_, i) => {
        const hourEl = dom.createElement("div", "hour-block");
        hourEl.innerHTML = `<span>${formatIndexToTime(i)}</span>`;
        timeWrapperEl.appendChild(hourEl);
      });
  }

  _createAgendaDisplay(bodyEl) {
    const displayEl = dom.createElement("div", "agenda-display-wrapper");
    bodyEl.appendChild(displayEl);

    const initialDate = this._initialDate();

    Array(this.agendaColumns)
      .fill()
      .map((_, i) => {
        const date = fmt.add(initialDate, i);
        this._createRow(displayEl, date);
      });
  }

  _createRow(displayEl, date) {
    const formattedDate = fmt.shortDate(date);
    const rowEl = dom.createElement("div", "agenda-row");
    if (fmt.isWeekend(date)) rowEl.classList.add("is-weekend");
    if (fmt.isToday(date)) rowEl.classList.add("is-today");
    rowEl.setAttribute("date", formattedDate);
    rowEl.addEventListener("click", e =>
      this._handleDayClick(e, formattedDate)
    );

    const events = this.events.filter(e => {
      return (
        fmt.midnight(e.start * 1000).getTime() === fmt.midnight(date).getTime()
      );
    });

    this._displayEvents(events, rowEl, date);

    Array(24)
      .fill()
      .map((_, i) => {
        const rowHourEl = dom.createElement("div", "agenda-row-hour-block");
        rowEl.appendChild(rowHourEl);
      });
    displayEl.appendChild(rowEl);
  }

  _displayEvents(events, rowEl, date) {
    if (events.length === 0) return;

    events.map(e => {
      const eventEl = dom.createElement("div", "event");
      eventEl.addEventListener("click", event =>
        this._handleEventClick(event, e)
      );

      const heightPerMinute = this.agendaBlockHeight / 60;

      eventEl.innerHTML = `<div class="event-title">${e.name}</div>`;

      const eventDayMinutes =
        (e.start - fmt.midnight(e.start * 1000) / 1000) / 60;
      const elTop = eventDayMinutes * heightPerMinute;
      eventEl.style.left = "1%";
      eventEl.style.top = `${elTop}px`;
      eventEl.style.borderLeft = `solid 3px ${e.color}`;

      rowEl.appendChild(eventEl);
    });
  }

  createWeekEvents(dayRowEl, events) {
    const eventsWrapper = dom.createElement("div", "events-wrapper");
    dayRowEl.appendChild(eventsWrapper);

    let eventHeights = [];
    events.map(e => {
      const eventEl = dom.createElement("div", "event");
      eventEl.setAttribute("event", e.id);
      eventEl.addEventListener("click", event =>
        this.handleEventClick(event, e)
      );
      const heightPerMinute = this.weekHourBlockHeight / 60;
      const eventDuration = (e.end - e.start) / 60;
      const elHeight = heightPerMinute * eventDuration;
      eventEl.style.height = `${elHeight}px`;
      eventEl.innerHTML = `<div class="event-title">${e.title}</div>`;
      const eventDayMinutes =
        (e.start -
          moment(e.start * 1000)
            .startOf("day")
            .unix()) /
        60;
      const elTop = eventDayMinutes * heightPerMinute;
      eventEl.style.left = `1%`;
      if (inArray(eventHeights, elTop)) {
        const matchingHeightsLen = eventHeights.filter(h => h === elTop).length;
        eventEl.style.left = `${randomIntFromInterval(10, 42)}%`;
      }
      eventHeights.push(elTop);
      eventEl.style.top = `${elTop}px`;
      eventEl.style.borderLeft = `solid 3px ${
        this.backgrounds[randomIntFromInterval(0, this.backgrounds.length - 1)]
      }`;
      eventsWrapper.appendChild(eventEl);
    });
  }

  _initialDate() {
    return this.agendaColumns != 7
      ? fmt.subtract(this.date, 1)
      : fmt.startOfWeek(this.date);
  }

  _handleDayClick(e, date) {
    const classes = e.target.classList;
    if (!classes.contains("agenda-row-hour-block")) return;
    window.dispatchEvent(
      new CustomEvent("calendar-day-click", {
        detail: {
          e: e,
          date
        }
      })
    );
  }

  _handleEventClick(e, event) {
    window.dispatchEvent(
      new CustomEvent("calendar-event-click", {
        detail: {
          e: e,
          event
        }
      })
    );
  }
}

export default Agenda;
