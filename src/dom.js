export default {
  createElement(type, ...classes) {
    const el = document.createElement(type);
    el.classList.add(...classes);
    return el;
  },

  getElement(selector) {
    return document.querySelector(selector);
  },

  getElements(selector) {
    return document.querySelectorAll(selector);
  },

  getVal(selector, radio = false) {
    if (!radio) return this.getElement(selector).value;
    const radios = document.getElementsByName(selector);
    let result = "";
    Array.from(radios).map(r => {
      if (r.checked === true) {
        result = r.value;
      }
    });

    return result;
  }
};
