import dom from "./dom";
import {
    formatIndexToTime,
    randomBetween
} from "./util";
import fmt from "./date";

export default class Month {
    constructor(element, date, events) {
        this.date = date;
        this.element = element;
        this.events = events;
        this.agendaBlockHeight = 49;
        this.agendaColumns = 7
        this.maxEvents = 4
    }

    render(rows) {
        this.element.innerHTML = '';

        const wrapper = dom.createElement('div', 'ta-calendar');
        this.element.appendChild(wrapper);

        const wrapperDiv = dom.createElement('div', 'ta-month-calendar-wrapper');
        wrapper.appendChild(wrapperDiv);

        const headerEl = dom.createElement('div', 'ta-month-header');
        wrapperDiv.appendChild(headerEl);
        this._createHeader(headerEl)

        const bodyEl = dom.createElement('div', 'ta-month-body');
        wrapperDiv.appendChild(bodyEl);
        this._createBody(bodyEl);
    }

    prev() {
        this.date = fmt.subtractMonths(this.date, 1)
        this.render();
    }

    next() {
        this.date = fmt.addMonths(this.date, 1)
        this.render();
    }

    setEvents(events) {
        this.events = events;
        this.render()
    }

    getTitle() {
        return `${fmt.getMonthName(this.date)} ${this.date.getFullYear()}`
    }

    _createHeader(headerEl) {
        const labels = ['Sun', 'Mon', 'Tue', "Wed", 'Thur', 'Fri', 'Sat'];
        labels.map(l => {
            const dayLabelEl = dom.createElement('div', 'ta-month-header-label');
            dayLabelEl.innerHTML = `${l}`
            headerEl.appendChild(dayLabelEl);
        })
    }

    _createBody(bodyEl) {
        const totalDaysInMonth = fmt.endOfMonth(this.date);
        const firstDayOfMonthWeekDate = fmt.startOfMonthDay(this.date)
        // In a 5 row calendar we can get 35 days. So if we need more than 35 days to display we know we will need 6 rows.
        const totalRows = (totalDaysInMonth + firstDayOfMonthWeekDate) > 35 ? 6 : 5;
        this._createRows(totalRows, bodyEl);
    }

    _createRows(count, bodyEl) {
        let date = fmt.startOfWeek(fmt.startOfMonth(this.date));
        Array(count).fill().map(() => {
            const rowDiv = dom.createElement('div', 'ta-month-row');
            Array(7).fill().map((_, i) => {
                this._createMonthDay(rowDiv, date);
                date = fmt.add(date, 1)
            })  
            bodyEl.appendChild(rowDiv);
        })
    }

    _createMonthDay(rowEL, date) {
        const dayEl = dom.createElement('div', 'ta-month-day');
        if (fmt.isToday(date)) {
            dayEl.classList.add('ta-today');
        }
        
        if (!fmt.currentMonth(this.date, date)) {
            dayEl.classList.add('ta-other-month');
        }
        const dayFormatted = fmt.shortDate(date)
        dayEl.setAttribute('ta-date', dayFormatted);
        dayEl.addEventListener('click', e => this._handleDayClick(e, dayFormatted));

        const numEl = dom.createElement('div', 'ta-month-day-number');
        numEl.innerHTML = fmt.dateNumber(date);
        dayEl.appendChild(numEl);

        const eventsDayWrapperEl = dom.createElement('div', 'ta-month-day-events');
        if (this.events && this.events.length > 0) {
            const events = this.events.filter(e => fmt.shortDate(new Date(e.start * 1000)) === dayFormatted);
            this._displayEvents(eventsDayWrapperEl, events);
        }
        dayEl.appendChild(eventsDayWrapperEl);
        rowEL.appendChild(dayEl);
    }

    _displayEvents(eventsWrapperEl, events) {
        const eventsLen = events.length;
        let maxEvents = eventsLen;
        let remainingEvents = 0;
        if (eventsLen > this.maxEvents) {
            maxEvents = this.maxEvents;
            remainingEvents = eventsLen - maxEvents;
        }

        for (let e = 0; e < maxEvents; e++) {
            const event = events[e];
            const eventEl = dom.createElement('div', 'ta-event');
            eventEl.addEventListener('click', (v) => this._handleEventClick(v, event));
            eventEl.setAttribute('ta-event-id', event.id);
            eventEl.innerHTML = event.title;
            eventEl.style.borderLeft = `solid 2px ${this.backgrounds[randomBetween(0, this.backgrounds.length - 1)]}`
            eventsWrapperEl.appendChild(eventEl);
        }

        if (remainingEvents > 0) {
            const loadMoreEvents = dom.createElement('div', 'ta-more-events');
            loadMoreEvents.innerHTML = `${remainingEvents} more`;
            eventsWrapperEl.appendChild(loadMoreEvents);
        }
    }

    _handleDayClick(e, date) {
        window.dispatchEvent(
          new CustomEvent("calendar-day-click", {
            detail: {
              e: e,
              date
            }
          })
        );
      }
    
      _handleEventClick(e, event) {
        window.dispatchEvent(
          new CustomEvent("calendar-event-click", {
            detail: {
              e: e,
              event
            }
          })
        );
      }
}