# Agenda Code Interview

Here is a [DEMO](https://interview-code-28071.firebaseapp.com/) of the application.

## Install
```bash
yarn install 
// or
npm i
```

## Run the application
```bash
yarn start
// or
npm start
```